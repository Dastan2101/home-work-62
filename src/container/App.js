import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Hotels from "../components/Hotels/Hotels";
import Flights from "../components/Flights/Flights";
import MainPage from "../components/MainPage/MainPage";
import BestDeals from "../components/BestDeals/BestDeals";

class App extends Component {
  render() {
    return (
      <div className="App">
          <BrowserRouter>
              <Switch>
                  <Route path="/" exact component={MainPage}/>
                  <Route path="/hotels" component={Hotels}/>
                  <Route path="/flights" component={Flights}/>
                  <Route path="/deals" component={BestDeals}/>
              </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
