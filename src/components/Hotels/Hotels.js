import React, {Component, Fragment} from 'react';
import './Hotels.css'
import { Row, Col } from 'reactstrap';
import apartments from '../../assets/apartmens.jpg';
import resorts from '../../assets/Resorts.jpg';
import villas from '../../assets/villas.jpg';
import cabins from '../../assets/cabins.jpg';
import cottage from '../../assets/cottages.jpg';
import Header from "../Header/Header";


class Hotels extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <div className="search-div">
                <div className="container">
                        <h2>Find deals for any season</h2>
                        <h6>From cosy country homes to funky city flats</h6>
                    <form className="form">
                        <input type="text" placeholder="Where are you going?"/>
                        <input type="date"/>
                        <button type="button" className="btn-search">Search</button>
                    </form>

                </div>
                </div>
                <div className="hotels container">
                    <Row>
                        <Col xs="3">
                            <img src={apartments} alt="" style={{width: '200px', height: '180px'}}/>
                            <h5>Apartments</h5>
                            <p>696,991 apartments</p>
                        </Col>
                        <Col xs="3">
                            <img src={resorts} alt="" style={{width: '200px', height: '180px'}}/>
                            <h5>Resorts</h5>
                            <p>21,001 resorts</p>
                        </Col>
                        <Col xs="3">
                            <img src={villas} alt="" style={{width: '200px', height: '180px'}}/>
                            <h5>Villas</h5>
                            <p>354,226 villas</p>
                        </Col>
                        <Col xs="3">
                            <img src={cabins} alt="" style={{width: '200px', height: '180px'}}/>
                            <h5>Cabins</h5>
                            <p>12,542 cabins</p>
                        </Col>
                        <Col xs="3">
                            <img src={cottage} alt="" style={{width: '200px', height: '180px'}}/>
                            <h5>Cottages</h5>
                            <p>114,547 cottages</p>
                        </Col>

                    </Row>
                </div>
            </Fragment>
        );
    }
}

export default Hotels;