import React, {Component, Fragment} from 'react';
import Header from "../Header/Header";
import './Flights.css';
import Button from "reactstrap/es/Button";
class Flights extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
            <div className="flights-div">
                <div className="container flight">
                    <h4 className="title-flights">Book cheap flight deals</h4>
                    <div className="flights-form">
                    <form>
                    <input type="text" placeholder="From" className="input"/>
                    <input type="text" placeholder="To" className="input"/>
                    </form>
                    <form>
                    <input type="number" placeholder="Departure" className="input"/>
                    <input type="number" placeholder="Return" className="input"/>
                    </form>
                        <Button color="danger" size="lg" active>Search Flights</Button>
                </div>
                </div>
            </div>
            </Fragment>
        );
    }
}

export default Flights;