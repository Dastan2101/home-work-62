import React, {Component, Fragment} from 'react';
import './BestDeals.css';
import dubai from "../../assets/dubai.jpg";
import {Button} from "reactstrap";
import phuket from "../../assets/phuket.jpg";
import maldives from "../../assets/maldivy.jpg";
import singapore from "../../assets/singapur.jpg";
import bali from "../../assets/bali.jpg";
import goa from "../../assets/goa.jpg";
import Header from "../Header/Header";

class BestDeals extends Component {
    render() {
        return (
            <div>
                <Fragment>
                    <Header/>
                    <div className="tours">
                        <h2 className="title-tour">Best Deals</h2>
                        <div className="container">
                            <div className="tour-boxes">
                                <div className="tour">
                                    <img src={dubai} alt="#" className="tour-img"/>
                                    <h5>Anantara The Palm Dubai Resort</h5>
                                    <p>17187 KGS</p>
                                    <Button color="primary" className="tour-btn" size="lg" block>Show</Button>

                                </div>
                                <div className="tour">
                                    <img src={phuket} alt="#" className="tour-img"/>
                                    <h5>Splash Beach Resort by Langham Hospitality Group</h5>
                                    <p>24242 KGS</p>
                                    <Button color="primary" className="tour-btn" size="lg" block>Show</Button>
                                </div>
                                <div className="tour">
                                    <img src={maldives} alt="#" className="tour-img"/>
                                    <h5>Filitheyo Island Resort</h5>
                                    <p>17187 KGS</p>
                                    <Button color="primary" className="tour-btn" size="lg" block>Show</Button>
                                </div>
                                <div className="tour">
                                    <img src={singapore} alt="#" className="tour-img"/>
                                    <h5>Hotel Jen Orchardgateway Singapore</h5>
                                    <p>16865 KGS</p>
                                    <Button color="primary" className="tour-btn" size="lg" block>Show</Button>
                                </div>
                                <div className="tour">
                                    <img src={bali} alt="#" className="tour-img"/>
                                    <h5>Kuta Seaview Boutique Resort & Spa</h5>
                                    <p>25643 KGS</p>
                                    <Button color="primary" className="tour-btn" size="lg" block>Show</Button>
                                </div>
                                <div className="tour">
                                    <img src={goa} alt="#" className="tour-img"/>
                                    <h5>The Park Calangute Goa</h5>
                                    <p>12627 KGS</p>
                                    <Button color="primary" className="tour-btn" size="lg" block>Show</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Fragment>
            </div>
        );
    }
}

export default BestDeals;