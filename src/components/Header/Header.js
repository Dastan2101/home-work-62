import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
    return (
            <div className="nav-div">
                <div className="container">
                    <NavLink to='/' className="logo">Just Travel</NavLink>
                    <ul className="main-nav">
                        <NavLink to='/'>Home</NavLink>
                        <NavLink to='/deals'>Best Deals</NavLink>
                        <NavLink to='/hotels'>Hotels</NavLink>
                        <NavLink to='/flights'>Flights</NavLink>
                    </ul>
                </div>

            </div>
    );
};

export default Header;